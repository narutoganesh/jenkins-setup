package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.SnippetType;


@CucumberOptions(
	
	features = "src/test/java/feature/Login.feature" 
	,glue = {"stepImpl", "pages"},
	monochrome=true
	/*dryRun =true,
	snippets = SnippetType.CAMELCASE*/
)
@RunWith(Cucumber.class)
	
	public class RunTest {
	
	
}
