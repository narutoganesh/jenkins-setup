package weekday4day1assign;

import org.openqa.selenium.chrome.ChromeDriver;

public class PhoneX {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.findElementByXPath("//div[@class='_3Njdz7']/button").click();
		driver.findElementByClassName("LM6RPg").sendKeys("iPhonex");
		driver.findElementByClassName("vh79eN").click();
		String text = driver.findElementByXPath("(//div[@class='_1uv9Cb']/div)[1]").getText();
		System.out.println("The Price of second mobile is"+text);
		
	}

}
