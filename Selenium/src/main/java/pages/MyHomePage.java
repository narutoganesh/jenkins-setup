package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import week4day2.ProjectMethods;

public class MyHomePage extends ProjectMethods {

public MyHomePage() {
	PageFactory.initElements(driver, this);
	
}

@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogout;
@FindBy(how =How.LINK_TEXT , using ="Leads") WebElement eleLeads;

//Click on Leads
@And ("Click on My Lead")
public MyLeads clickLeads() {
	click(eleLeads);
	return new MyLeads();
}


//click on Logout
public LoginPage clickLogin() {
	click(eleLogout);
	return new LoginPage();
	
}

}
