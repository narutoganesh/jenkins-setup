package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import week4day2.ProjectMethods;

public class MyLeads extends ProjectMethods {

public MyLeads() {
	PageFactory.initElements(driver, this);
	
}

@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogout;
@FindBy(how =How.LINK_TEXT , using ="Leads") WebElement eleLeads;
@FindBy(how =How.LINK_TEXT , using ="Create Lead") WebElement eleCreateLead;



//Click on Create Lead
@And ("Click on Create Lead")
public CreateLead clickCreateLead()
{
	
	click(eleCreateLead);
	return new CreateLead();
}

//click on Logout
public LoginPage clickLogin(String logout) {
	type(eleLogout, logout);
	return new LoginPage();
	
}

}
