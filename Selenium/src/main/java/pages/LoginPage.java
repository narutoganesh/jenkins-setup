package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import week4day2.ProjectMethods;

public class LoginPage extends ProjectMethods {

public LoginPage() {
	PageFactory.initElements(driver, this);
	
}

@FindBy(how = How.ID , using ="username") WebElement eleUserName;
@FindBy(how = How.ID , using ="password") WebElement elePassword;
@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogin;

//Enter user name
@And ("Enter the username as (.*)")
public LoginPage enterUsername(String uName) {

	type(eleUserName, uName);
	return this;
	
}

//Enter Password
@And ("Enter the password as (.*)")
public LoginPage enterPassword(String password) {
	type(elePassword, password);
	return this;
	
}

//click on Login button
@When ("Click on Login button")
		 
public HomePage clickLogin() {
	click(eleLogin);
	return new HomePage();
	
}

}
