package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import week4day2.ProjectMethods;

public class CreateLead extends ProjectMethods {

public CreateLead() {
	PageFactory.initElements(driver, this);
	
}

@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogout;
@FindBy(how =How.ID , using ="createLeadForm_companyName") WebElement elecname;
@FindBy(how =How.ID , using ="createLeadForm_lastName") WebElement elelname;
@FindBy(how =How.ID , using ="createLeadForm_firstName") WebElement elefname;
@FindBy(how =How.CLASS_NAME , using ="smallSubmit") WebElement eleSubmit;

//Enter Company name
@And ("Enter Company Name as(.*)")
public CreateLead entercname(String companyName) {
	type(elecname,companyName);
	return this;
}

//Enter First name

@And ("Enter FirstName as(.*)")
public CreateLead enterfname(String firstName) {
	type(elefname, firstName);
	return this;
}

//Enter Last name
@And ("Enter Last Name as(.*)")
public CreateLead enterlname(String lastName) {
	type(elelname, lastName);
	return this;
}

//Click on Create Lead button to create a new lead

@And ("click on Submit")
public ViewLead clickCreateLeadButton() {
	click(eleSubmit);
	return new ViewLead();
	
}

//click on Logout
public LoginPage clickLogin() {
	click(eleLogout);
	return new LoginPage();
	
}

}
