package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import week4day2.ProjectMethods;

public class HomePage extends ProjectMethods {

public HomePage() {
	PageFactory.initElements(driver, this);
	
}

@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogout;
@FindBy(how =How.LINK_TEXT , using ="CRM/SFA") WebElement eleCRMSFA;

//Click on CRMSFA

@And ("Click on CRMSFA")	
public MyHomePage clickCRM() {
	click(eleCRMSFA);
	return new MyHomePage();
}
//click on Logout
public LoginPage clickLogout() {
	click(eleLogout);
	return new LoginPage();
	
}

}
