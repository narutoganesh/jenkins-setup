package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week4day2.ProjectMethods;

public class ViewLead extends ProjectMethods {

public ViewLead() {
	PageFactory.initElements(driver, this);
	
}

@FindBy(how = How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogout;
@FindBy(how = How.ID, using ="viewLead_firstName_sp") WebElement eleVerifyName;




//Verify the lead created

public void VerifyLead()
{
	//write lead code
}
//click on Logout
public LoginPage clickLogin(String logout) {
	type(eleLogout, logout);
	return new LoginPage();
	
}

}
