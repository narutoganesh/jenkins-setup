package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import week4day2.SeMethods;

public class LearnExcel extends SeMethods {
	
//@Test


public static Object[][]  excel(String dataSheetName) throws IOException
{
	XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
    XSSFSheet sheet = wb.getSheetAt(0);
    //XSSFRow row = sheet.getRow(0);
    //Getting Row Count
    int rowCount = sheet.getLastRowNum();
    System.out.println(rowCount);
   // XSSFCell cell = row.getCell(0);
    //Header Count
    int cellCount = sheet.getRow(0).getLastCellNum();
    System.out.println(cellCount);
    Object[][] data = new Object[rowCount][cellCount];
    for(int i=1;i<=rowCount;i++)
    {
    	XSSFRow row = sheet.getRow(i);   //Reading Specific row
    	
    	for(int j=0;j<cellCount;j++)
    		
    	{
    		XSSFCell cell = row.getCell(j); //Reading Specific cell value
    		try {
				String cellValue = cell.getStringCellValue(); // Getting the value in the cell and storing to variable
				System.out.println(cellValue);
				data[i-1][j]=cellValue;

			} catch (NullPointerException e) {
				System.out.println("");
			}
    		
    	}
    		

    }
    
    wb.close();
    return data;
}

 
}
