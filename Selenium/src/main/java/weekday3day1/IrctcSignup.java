package weekday3day1;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcSignup {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementById("userRegistrationForm:userName").sendKeys("naruto");
		driver.findElementById("userRegistrationForm:password").sendKeys("password123");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("password123");
		WebElement security = driver.findElementById("userRegistrationForm:securityQ");
		Select dd = new Select(security);
		dd.selectByVisibleText("What is your all time favorite sports team?");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Manchester United");
		WebElement lang = driver.findElementById("userRegistrationForm:prelan");
		Select lg = new Select(lang);
		lg.selectByValue("en");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Anandha Ganesh");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("K");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Arulananda Gopal");
		WebElement radio = driver.findElement(By.id("userRegistrationForm:gender:0"));
		radio.click();
		WebElement marital = driver.findElement(By.id("userRegistrationForm:maritalStatus:0"));
		marital.click();
		WebElement dobday = driver.findElementById("userRegistrationForm:dobDay");
		Select dobd = new Select(dobday);
		dobd.selectByVisibleText("04");
		WebElement dobmon = driver.findElementById("userRegistrationForm:dobMonth");
		Select dobm = new Select(dobmon);
		dobm.selectByValue("11");
		WebElement dobyear = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select doby = new Select(dobyear);
		doby.selectByValue("1990");
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select occ = new Select(occupation);
		occ.selectByVisibleText("Private");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select cont = new Select(country);
		cont.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("narutoganesh90@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9677275146");
		driver.findElementById("userRegistrationForm:address").sendKeys("PB Emerald Flats");
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select nat = new Select(nationality);
		nat.selectByVisibleText("India");
		driver.findElementById("userRegistrationFor5m:pincode").sendKeys("600061",Keys.TAB);
		Thread.sleep(5000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select cit = new Select(city);
		cit.selectByVisibleText("Kanchipuram");
		Thread.sleep(5000);
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select po = new Select(post);
		po.selectByVisibleText("Nanganallur Bazaar S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("4445943589");
	

	}

}
